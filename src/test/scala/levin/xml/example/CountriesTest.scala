package levin.xml.example

import levin.xml.XmlMatchers
import org.scalatest.{FunSuite, Matchers}

import scala.language.postfixOps
import scala.xml.{Elem, Node, XML}

/**
  * @author eugene 
  * @since 07/02/16.
  */
class CountriesTest extends FunSuite with XmlMatchers with Matchers {

  val xml: Elem = XML.loadFile(getClass.getResource("/countries.xml").getPath)

  test("find highest and lowest densities countries") {
    val countries = xml \ "country" sortBy getDensity
    val minDensityCountry = countries head
    val maxDensityCountry = countries last

    val result =
      <result>
        <highest density={getDensity(maxDensityCountry) toString}>{maxDensityCountry \@ "name"}</highest>
        <lowest density={getDensity(minDensityCountry) toString}>{minDensityCountry \@ "name"}</lowest>
      </result>

    println (result)
  }

  private def getDensity(country: Node): Double = {
    (country \@ "population").toFloat / (country \@ "area").toFloat
  }

}
