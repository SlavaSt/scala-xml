package levin.xml.example

import levin.xml.XmlMatchers
import org.scalatest.{FunSuite, Matchers}

import scala.collection.immutable.Seq
import scala.language.postfixOps
import scala.xml.{Elem, Node, NodeSeq, XML}

/**
  * @author eugene 
  * @since 07/02/16.
  */
class BookStoreTest extends FunSuite with XmlMatchers with Matchers {

  val bookStore: Elem = XML.loadFile(getClass.getResource("/books.xml").getPath)

  test("load xml from file") {
    bookStore should not be null
  }

  test("select all the titles") {
    val titles = bookStore \ "book" \ "title"

    titles.toList should equal(List(
      <title lang="en">Everyday Italian</title>,
      <title lang="en">Harry Potter</title>,
      <title lang="en">XQuery Kick Start</title>,
      <title lang="en">Learning XML</title>
    ))
  }

  test("select cheaper than 30") {
    val books: NodeSeq = bookStore \ "book" filter (book => (book \ "price").text.toFloat < 30)

    books shouldEqual
      <book category="CHILDREN">
        <title lang="en">Harry Potter</title>
        <author>J K. Rowling</author>
        <year>2005</year>
        <price>29.99</price>
      </book>
  }

  test("titles of books more expensive than 30") {
    val titles: Seq[NodeSeq] =
      for (book <- bookStore \ "book" if (book \ "price").text.toFloat > 30)
        yield book \ "title" head

    titles.toList should equal(List(
      <title lang="en">XQuery Kick Start</title>,
      <title lang="en">Learning XML</title>
    ))
  }

  test("titles of books more expensive than 30 sorted by title") {
    val titles: Seq[NodeSeq] =
      for (book <- bookStore \ "book" if (book \ "price").text.toFloat > 30)
        yield book \ "title" head
    val titlesSorted = titles.sortBy(_.text)

    titlesSorted.toList should equal(List(
      <title lang="en">Learning XML</title>,
      <title lang="en">XQuery Kick Start</title>
    ))
  }

  test("present result as html list") {
    val titles: Seq[NodeSeq] = bookStore \ "book" \ "title" sortBy (_.text)

    val result =
      <ul>
        {for (title <- titles) yield <li>
        {title}
      </li>}
      </ul>

    result shouldEqual
      <ul>
        <li>
          <title lang="en">Everyday Italian</title>
        </li>
        <li>
          <title lang="en">Harry Potter</title>
        </li>
        <li>
          <title lang="en">Learning XML</title>
        </li>
        <li>
          <title lang="en">XQuery Kick Start</title>
        </li>
      </ul>
  }

  test("present result as html list - eliminate title element") {
    val titles: Seq[String] = bookStore \ "book" \ "title" map (_.text) sorted

    val result =
      <ul>
        {for (title <- titles) yield <li>
        {title}
      </li>}
      </ul>

    result shouldEqual
      <ul>
        <li>Everyday Italian</li>
        <li>Harry Potter</li>
        <li>Learning XML</li>
        <li>XQuery Kick Start</li>
      </ul>
  }

  test("present books by categories - children/adult") {
    val books = bookStore \ "book"

    val result: Seq[Node] = books.map { book =>
      val title = book \ "title" text;
      book \@ "category" match {
        case "CHILDREN" => <child>
          {title}
        </child>
        case _ => <adult>
          {title}
        </adult>
      }
    }

    result.toList should equal(List(
      <adult>Everyday Italian</adult>,
      <child>Harry Potter</child>,
      <adult>XQuery Kick Start</adult>,
      <adult>Learning XML</adult>
    ))
  }

  test("present as html") {
    val books = bookStore \ "book" sortBy (_ \ "title" text)
    val result: Seq[Node] =
      <html>
        <body>
          <h1>Bookstore</h1>
          <ul>
            {
              for (book <- books) yield
                <li>
                  {book \ "title" text}. Category: {book \@ "category"}
                </li>
            }
          </ul>
        </body>
      </html>

    result shouldEqual
      <html>
        <body>

          <h1>Bookstore</h1>

          <ul>
            <li>Everyday Italian. Category: COOKING</li>
            <li>Harry Potter. Category: CHILDREN</li>
            <li>Learning XML. Category: WEB</li>
            <li>XQuery Kick Start. Category: WEB</li>
          </ul>

        </body>
      </html>

  }

  test("add attributes") {
    val books = bookStore \ "book" sortBy (_ \ "title" text)
    val result: Seq[Node] = <html>
      <body>
        <h1>Bookstore</h1>

        <ul>
          {
            for (book <- books) yield
              <li class={book \@ "category"}>{book \ "title" text}</li>
          }
        </ul>

      </body>
    </html>

    result shouldEqual <html>
      <body>
        <h1>Bookstore</h1>

        <ul>
          <li class="COOKING">Everyday Italian</li>
          <li class="CHILDREN">Harry Potter</li>
          <li class="WEB">Learning XML</li>
          <li class="WEB">XQuery Kick Start</li>
        </ul>

      </body>
    </html>
  }

}
