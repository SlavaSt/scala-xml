package levin

import org.custommonkey.xmlunit.{XMLUnit, XMLAssert}
import org.scalatest.Matchers

import scala.xml.Node

/**
  * @author eugene 
  * @since 07/02/16.
  */
package object xml {

  implicit class shouldUtil(actual: Seq[Node]) extends Matchers {
    def shouldEqual(expected: Seq[Node]): Unit = {
      XMLUnit.setIgnoreWhitespace(true)
      val n1: String = actual.mkString.trim
      val n2: String = expected.mkString.trim
      if (n1.isEmpty && !n2.isEmpty) {
        assert(n1 === n2)
      } else {
        XMLAssert.assertXMLEqual(n1, n2)
      }
    }
  }

}
