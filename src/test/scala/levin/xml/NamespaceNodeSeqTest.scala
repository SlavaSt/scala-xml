package levin.xml

import com.felstar.xqs.XQS._
import com.saxonica.xqj.SaxonXQDataSource
import org.scalatest.{Matchers, FunSuite}

import scala.xml._
import levin.xml.NamespaceNodeSeq._


/**
  * @author eugene
  * @since 04/02/16.
  */
class NamespaceNodeSeqTest extends FunSuite with Matchers {

  val source= new SaxonXQDataSource
  val conn = source.getConnection

  val b = Namespace("b.com")
  val c = Namespace("c.com")

  test("test xquery") {
    val hi: Seq[Elem] = conn.apply(
      """
        |declare namespace b = "b.com";
        |
        |let $a := <hello><b:hi/></hello>
        |return $a/b:hi
      """.stripMargin)

    hi shouldEqual <hi xmlns="b.com"/>
  }

  test("test xquery in scala context") {
    val hello: Seq[Elem] = conn.apply("/hello", <hello><hi/></hello>)

    hello shouldEqual <hello><hi/></hello>
    hello shouldEqual <hello><hi></hi></hello>
  }

  test("should find first node with namespace") {
    val xml: Elem = <hello xmlns:b="c.com" xmlns="b.com"><b:hi>some text</b:hi></hello>

    xml \ b("hello") shouldEqual xml
  }

  test("should find second node with namespace") {
    val xml: Elem = <hello xmlns:b="c.com" xmlns="b.com"><b:hi>some text</b:hi></hello>

    xml \ b("hello") \ c("hi") shouldEqual <b:hi xmlns:b="c.com">some text</b:hi>
  }

  test("should be namespace prefix agnostic") {
    val xml: Elem = <hello xmlns:b="c.com" xmlns="b.com"><b:hi>some text</b:hi></hello>

    xml \ b("hello") \ c("hi") shouldEqual <d:hi xmlns:d="c.com">some text</d:hi>
  }

  test("should not find node with wrong namespace") {
    val xml: Elem = <hello xmlns:a="b.com"><a:hi>some text</a:hi></hello>

    xml \ c("hello") shouldBe empty
  }

  test("should find node using wildcard namespace") {
    val xml: Elem = <hello xmlns:a="b.com"><a:hi>some text</a:hi></hello>

    xml \ *("hello") shouldEqual <hello><a:hi xmlns:a="b.com">some text</a:hi></hello>
  }

  test("should find node with default namespace") {
    val xml: Elem = <hello xmlns:a="b.com"><a:hi>some text</a:hi></hello>

    xml \ default("hello") shouldEqual <hello><a:hi xmlns:a="b.com">some text</a:hi></hello>
  }

  test("test implicit namespaces") {
    val xml1: Elem = <hello xmlns="b.com"><hi>some text</hi></hello>
    val xml2: Elem = <hello xmlns="c.com"><hi>some text</hi></hello>

    implicit val implicitNamespace: Namespace = Namespace("b.com")
    xml1.ns \ "hello" shouldEqual <hello xmlns="b.com"><hi>some text</hi></hello>
    xml2.ns \ "hello" shouldBe empty
  }

  test("test implicit default namespace") {
    val xml1: Elem = <hello><hi>some text</hi></hello>

    implicit val implicitNamespace: Namespace = __

    xml1.ns \ "hello" shouldEqual <hello><hi>some text</hi></hello>
  }

  test("should behave correctly") {
    val xml1: Elem = <hello xmlns="b.com" xmlns:c="c.com"><c:hi>some text</c:hi></hello>

    xml1 \ *("hello") shouldEqual <hello xmlns="b.com" xmlns:c="c.com"><c:hi>some text</c:hi></hello>
    xml1 \ __("hello") shouldBe empty
    xml1 \ b("hello") shouldEqual <hello xmlns="b.com"><c:hi xmlns:c="c.com">some text</c:hi></hello>
    xml1 \ b("hello") \ c("hi") shouldEqual <c:hi xmlns:c="c.com">some text</c:hi>
    xml1 \ b("hello") \ b("hi") shouldBe empty

    val xml2: Elem = <hello xmlns:c="c.com"><c:hi>some text</c:hi></hello>

    xml2 \ *("hello") shouldEqual <hello xmlns:c="c.com"><c:hi>some text</c:hi></hello>
    xml2 \ __("hello") shouldEqual <hello xmlns:c="c.com"><c:hi>some text</c:hi></hello>
  }


  test("should map to another node") {
    val xml: Elem = <b:hello xmlns:b="b.com"><b:hi>some text</b:hi></b:hello>

    val mapped = xml \ b("hello") map (_ => <hello></hello>)

    mapped shouldEqual <hello/>
  }

}

