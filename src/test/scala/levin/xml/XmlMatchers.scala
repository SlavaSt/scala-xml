package levin.xml

import org.custommonkey.xmlunit.{XMLAssert, XMLUnit}
import org.scalatest.Matchers

import scala.xml.Node

/**
  * @author eugene 
  * @since 07/02/16.
  */
trait XmlMatchers {

  implicit class shouldUtil(actual: Seq[Node]) extends Matchers {
    def shouldEqual(expected: Seq[Node]): Unit = {
      XMLUnit.setIgnoreWhitespace(true)
      val n1: String = actual.mkString.trim
      val n2: String = expected.mkString.trim
      if (n1.isEmpty && !n2.isEmpty) {
        assert(n1 === n2)
      } else {
        XMLAssert.assertXMLEqual(n1, n2)
      }
    }
  }

}
