package levin.xml

import org.scalatest.{Matchers, FunSuite}

/**
  * @author eugene 
  * @since 07/02/16.
  */
class NSNodeTest extends FunSuite with Matchers {

  test("should not allow null label") {
    intercept[NullPointerException] {
      NamespaceNode(Namespace("namespace"), null)
    }
  }

  test("null namespace is fine - it means default (empty) namespace") {
    val ns = NamespaceNode(null, "label")

    ns.label shouldEqual "label"
  }

}
