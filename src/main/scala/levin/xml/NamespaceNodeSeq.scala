package levin.xml

import scala.collection.mutable.ListBuffer
import scala.collection.{AbstractSeq, SeqLike, immutable, mutable}
import scala.language.implicitConversions
import scala.xml._

/**
  * @author eugene 
  * @since 04/02/16.
  */
case class NamespaceNodeSeq(nodeSeq: NodeSeq) extends AbstractSeq[Node] with immutable.Seq[Node] with SeqLike[Node, NamespaceNodeSeq] with Equality {

  def ns: NamespaceNodeSeq = this

  def \(that: NamespaceNode): NamespaceNodeSeq = {
    nodeSeq match {
      case e: Elem =>
        if (e.label != that.label) NodeSeq.Empty
        else if (that.namespace.eq(*) || e.namespace == that.namespace.uri) Group(e)
        else NodeSeq.Empty
      case _ => doFilteringNamespace(that) {
        _ \ that.label
      }
    }
  }

  def \(label: String)(implicit namespace: Namespace): NamespaceNodeSeq = this \ new NSNode(namespace, label)

  def \\(that: NamespaceNode): NamespaceNodeSeq = {
    doFilteringNamespace(that) {
      _ \\ that.label
    }
  }

  def \\(label: String)(implicit namespace: Namespace): NamespaceNodeSeq = this \\ new NSNode(namespace, label)

  private def doFilteringNamespace(that: NamespaceNode)(operation: NodeSeq => NodeSeq): NamespaceNodeSeq = {
    that match {
      case NamespaceNode(*, _) => operation(nodeSeq)
      case _ => operation(nodeSeq) filter {
        _.namespace == that.namespace.uri
      }
    }
  }

  override protected[this] def newBuilder: mutable.Builder[Node, NamespaceNodeSeq] = new ListBuffer[Node] mapResult { case nodes: List[Node] => NamespaceNodeSeq(NodeSeq.fromSeq(nodes)) }

  override def toString: String = nodeSeq.toString

  override def length: Int = nodeSeq.length

  override protected def basisForHashCode: Seq[Any] = nodeSeq

  override def strict_==(other: Equality): Boolean = {
    other match {
      case o: NamespaceNodeSeq => nodeSeq == o.nodeSeq
      case _ => false
    }
  }

  override def iterator: Iterator[Node] = nodeSeq.iterator

  override def apply(idx: Int) = nodeSeq.apply(idx)

}

object NamespaceNodeSeq {

  implicit def nodeSeq2NsNodeSeq(nodeSeq: NodeSeq): NamespaceNodeSeq = NamespaceNodeSeq(nodeSeq)

  implicit def nsNodeSeq2nodeSeq(nsNodeSeq: NamespaceNodeSeq): NodeSeq = nsNodeSeq.nodeSeq

}
