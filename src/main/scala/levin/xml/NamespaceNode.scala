package levin.xml

import java.util.Objects.requireNonNull

/**
  * @author eugene 
  * @since 04/02/16.
  */
private[xml] final case class NamespaceNode(namespace: Namespace, label: String) {
  requireNonNull(label)
}


