package levin

/**
  * @author eugene 
  * @since 07/02/16.
  */
package object xml {

  type NS = Namespace
  type NSNode = NamespaceNode
  type NSNodeSeq = NamespaceNodeSeq

}
