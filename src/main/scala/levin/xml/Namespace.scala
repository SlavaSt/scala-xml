package levin.xml

/**
  * @author eugene 
  * @since 05/02/16.
  */
case class Namespace(uri: String) {

  def apply(label: String) = new NamespaceNode(this, label)

}

/**
  * Any namespace
  */
object * extends Namespace("")


/**
  * Default a.k.a. empty namespace
  */
object default extends Namespace(null)

/**
  * Alias for default namespace
  */
object __ extends Namespace(null)

