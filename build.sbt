name := "scala-xml"

version := "1.0"

scalaVersion := "2.11.7"

scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation", "-Xlog-implicit-conversions")

resolvers ++= Seq(
  "jitpack" at "https://jitpack.io",
  "xqj" at "http://xqj.net/maven"
)

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-xml" % "1.0.5",
  "org.jdom" % "jdom2" % "2.0.6",
  "com.github.fancellu" % "xqs" % "v0.82"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.6",
  "xmlunit" % "xmlunit" % "1.6",
  "junit" % "junit" % "4.12"
) map (_ % "test")
